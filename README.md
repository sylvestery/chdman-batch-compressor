# CHDMAN Script

## What does it do

Uses the CHDMAN utility to batch compress and replace multiple BIN CUE Images.
Supports running  more than one job concurrently to speed up the process.

## Why

When I originally wrote this I needed to compress a bunch of bin/cue images and it wasn't easy to batch process folders with QTCHDMAN so I just wrote this script

## How to use

1. Place this script in the same place as CHDMAN 
2. Specify a directory for the image otherwise it will use the current directory
3. Use the -t flag to say how many chdman jobs to create.


## Links
[CHDMAN](https://www.mamedev.org/index.php)