import subprocess
import sys
import glob, os
from os import listdir
from multiprocessing.dummy import Pool
from threading import Thread

# This code is mostly obsolete now, it is easy to now just run chdman itself with Multiple jobs using QTCHDMAN

def search(path):
    tocompress = glob.glob(path + "./**/*.cue", recursive=True)
    return tocompress

# Retrieved from stack overflow to seperate list into chunks
def create_chunks(list, num):
    avg = len(list) / float(num)
    out = []
    last = 0.0

    while last < len(list):
        out.append(list[int(last):int(last + avg)])
        last+=avg
    print(out)
    return out

def compress(chunk):
    for file in chunk:
        outputfile = file.replace("cue", "chd") # CHDMAN Normally replaces the file
        subprocess.call(["chdman", "createcd", "-i", file, "-o", outputfile], creationflags=subprocess.CREATE_NEW_CONSOLE)

def main():
    threads = 1
    path = ""
    if len(sys.argv) >= 2:
        path = sys.argv[1]
    else:
        path = input("Enter a path (leave blank for current directory): ");
    if '-t' in sys.argv:
        threads  = int(sys.argv[3])
        print("Using % d threads"%(threads))
    elif '-h'  in sys.argv:
        print("Help \n-t Specify how many different chdman threads that the script should create.")
        return
    tocompress = search(path)
    print(tocompress)
    chunks = create_chunks(tocompress, threads)

    pool = Pool(threads)
    pool.map(compress, chunks)
    pool.close()
    pool.join()
    print("All jobs have been completed")
    input("Press enter to exit")
# Main function
main()